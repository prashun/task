<?php
/* 
 * Description: This function checks the validity of a default email for a given user ID.
 * 
 * Return Values:
 * -1: Indicates that the user ID is empty or default email is empty.
 *  0: Indicates that the default email is not valid.
 *  1: Indicates that the default email is valid.
 *  2: Indicates that the email address is empty or invalid.
 */

function checkDefaultEmailValid($userId = null) { 

    // Comment 1: Check if the userId is empty or null.
    if (empty($userId)) { 
        return -1; 
    } 

    // Comment 2: Get the default email for the given userId.
    $defaultEmail = $this->getDefaultEmailByUserId($userId); 

    // Comment 3: If the default email is empty, set a default email and retrieve it again.
    if (empty($defaultEmail)) { 
        $this->set_default_email($userId); 
        $defaultEmail = $this->getDefaultEmailByUserId($userId); 
    } 

    // Comment 4: If the default email is still empty, return -1.
    if (empty($defaultEmail)) { 
        return -1; 
    } 

    // Comment 5: Check if the default email is valid and was validated within the last 12 months.
    if ($defaultEmail['valid'] >= 1 && ($defaultEmail['validated_on'] > (time() - strtotime('-12 months')))) { 
        return 1; 
    } 

    // Comment 6: Get the email address from the default email data.
    $email = $defaultEmail['email']; 

    // Comment 7: Check if the email address is empty or invalid.
    if (empty($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)) { 
        return 2; 
    } 

    // Comment 8: Check if the email address is valid using an external method (checkIfValid).
    $validationResults = $this->checkIfValid($email); 

    // Comment 9: If the email address is not valid according to the external check, return 0. Otherwise, return 1.
    if (!$validationResults) { 
        return 0; 
    } else { 
        return 1; 
    } 
}
