<?php
namespace App\Http\Repository;

use App\Models\Profile;

class ProfileRepository {

    private $model;
    
    public function __construct(Profile $model)
    {
        $this->model = $model;
    }

    public  function search($query){
        return $this->model->with('email')->where('firstName','like',"%$query%")
                            ->orWhere('surName','like',"%$query%")
                            ->get();
    }
}