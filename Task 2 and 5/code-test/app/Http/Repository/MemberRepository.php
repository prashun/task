<?php
namespace App\Http\Repository;

use App\Models\Member;

class MemberRepository {
    private $model;

    public function __construct(Member $model)
    {
        $this->model = $model;
    }

    public function store($request){
        $data = $request->only($this->model->getFillable());

        $member = $this->model->create($data);

        if(isset($request->schools) && count($request->schools)){
            $member->schools()->sync($request->schools);
        }
        
        return $member;
    }

    public function update($id,$request){
        $member = $this->model->find($id);
        if(!$member){
            return;
        }

        $data = $request->only($this->model->getFillable());

        $member->update($data);

        if(isset($request->schools) && count($request->schools)){
            $member->schools()->sync($request->schools);
        }

        return $member;
    }

    public function find($id){
        return $this->model->find($id);
    }

    public function destroy($id){
        $member = $this->model->find($id);

        if($member){
            return $member->delete();
        }

        return false;
    }

}