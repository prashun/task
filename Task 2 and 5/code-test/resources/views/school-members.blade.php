@extends('layouts.app')

@section('title', 'School Search')

@section('content')
<div class="container mt-5">
    <div class="row">
        <div class="offset-md-10 col-2">
            <button type="button" class='btn btn-sm btn-success' id='export'>Export to CSV</button>
        </div>
    </div>
    <h1>School Members</h1>
    <table class='table table-stripped'>
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($members as $member)
                <tr>
                    <td>
                        {{$member->name}}
                    </td>
                    <td>
                        {{$member->email}}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    
</div>
@endsection

@push('script')
    <script>
        $(document).ready(function() {
            $('#export').on('click',function(){
                window.location.href = "{{route('school.members.export',$school_id)}}";
            });
        });
    </script>
@endpush