<?php

namespace App\Http\Controllers;

use App\Http\Repository\ProfileRepository;
use App\Http\Resources\ProfileResource;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    private $repo;

    public function __construct(ProfileRepository $repo){
        $this->repo = $repo;
    }
    
    public function search(Request $request){
        if(is_null($request->name)){
            return;
        }
        
        $result = $this->repo->search($request->name);
        
        return ProfileResource::collection($result);
    }
}
