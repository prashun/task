@extends('layouts.app')

@section('title', 'Task 5')

@section('content')
<div class="container mt-5">
    <h1>Registration Form</h1>
    <form method="POST" action="{{ route('members.store') }}">
        @csrf
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" id="name" name="name" class="form-control">
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" id="email" name="email" class="form-control">
                </div>
            </div>
        </div>        

        <div class="form-group">
            <label for="school">School</label>
            <select id="school" name="schools[]" class="form-control" id='multiselect' multiple>
                <option value="" disabled selected>Select schools</option>
                @foreach ($schools as $id => $name)
                    <option value="{{$id}}">{{$name}}</option>
                @endforeach
            </select>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection

