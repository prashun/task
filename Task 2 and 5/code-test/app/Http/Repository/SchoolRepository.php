<?php
namespace App\Http\Repository;

use App\Models\School;

class SchoolRepository {
    private $model;

    public function __construct(School $model)
    {
        $this->model = $model;
    }

    public function get(){
        return $this->model->pluck('school_name','id');
    }

    public function membersBySchool($school_id) {
        $school = $this->model->find($school_id);
        if($school){
            return $school->members;
        }
    }

}