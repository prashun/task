@extends('layouts.app')

@section('title', 'Task 2')

@section('content')
<div class="container mt-5">
    <h1>Search Profiles</h1>
    <form action="javascript:void(0)" class="form-inline" id="searchForm" enctype="multipart/form-data">
        <div class="form-group">
            <input type="text" name="name" class="form-control form-control-sm" placeholder="Name">
        </div>
        <button type="submit" class="btn btn-primary btn-sm ml-2">Search</button>
    </form>

    <table class="table" id='profileTable'>
        <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
            </tr>
        </thead>
        
        <tbody>

        </tbody>
    </table>
    
</div>
@endsection

@push('script')
<script>
    $(document).ready(function(){
        $('form').submit(function(){
            $.ajax({
                url: "{{route('profile.search')}}",
                type: 'GET',
                data: {
                    name: $("input[name='name']").val()
                },
                success: function({data}) {
                    let html = '';
                    $('#profileTable').find('tbody').html('');

                    if(!data){
                        html += `
                            <tr>
                                <td colspan='3' class='text-center'>No Records Found</td>
                            </tr>
                        `;

                        $('#profileTable').find('tbody').append(html);
                        return;
                    }

                    Object.values(data).forEach(profile => {
                        html += `
                            <tr>
                                <td>${profile.firstName}</td>
                                <td>${profile.lastName}</td>
                                <td>${profile.email}</td>
                            </tr>
                        `;
                    });

                    $('#profileTable').find('tbody').append(html);
                }
            });
        });
    });
</script>
    
@endpush