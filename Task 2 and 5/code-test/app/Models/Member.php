<?php

namespace App\Models;

use App\Models\School;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Member extends Model
{
    use HasFactory;

    protected $table = 'members';

    protected $fillable = ['name','email'];

    protected $appends = ['school_name'];

    public function schools(){
        return $this->belongsToMany(School::class,'member_has_schools','member_id','school_id');
    }

    public function getSchoolNameAttribute(){
        return implode(', ',$this->schools()->pluck('school_name')->toArray());
    }
    
}
