@extends('layouts.app')

@section('title', 'School Search')

@section('content')
<div class="container mt-5">
    <h1>Search Members</h1>
    <div class="row">
        <form action="{{route('school.members')}}" method="POST" class="form" id="searchForm" enctype="multipart/form-data">
            @csrf
            <div class="col-md-12">
                <div class="form-group">
                    <label for="school">School</label>
                    <select id="school" name="school" class="form-control" id='multiselect'>
                        <option value="" disabled selected>Select schools</option>
                        @foreach ($schools as $id => $name)
                            <option value="{{$id}}">{{$name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <button type="submit" class="btn btn-primary btn-sm ml-2">Search</button>
        </form>
    </div>
    
</div>
@endsection

@push('script')
<script>
    
</script>
    
@endpush