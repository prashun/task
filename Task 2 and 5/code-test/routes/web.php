<?php

use App\Http\Controllers\MemberController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\SchoolController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('task2');
});

Route::get('/search-profiles',[ProfileController::class,'search'])->name('profile.search');
Route::resource('members',MemberController::class);

Route::get('schools',[SchoolController::class,'searchBySchool'])->name('school.search');
Route::post('school/members',[SchoolController::class,'members'])->name('school.members');
Route::get('member/export/{school_id}',[SchoolController::class,'export'])->name('school.members.export');

