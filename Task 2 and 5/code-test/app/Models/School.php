<?php

namespace App\Models;

use App\Models\Member;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class School extends Model
{
    use HasFactory;

    public function members(){
        return $this->belongsToMany(Member::class,'member_has_schools','school_id','member_id');
    }
}
