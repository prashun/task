<?php

use Database\Seeders\SchoolSeeder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->id();
            $table->string('school_name');
            $table->timestamps();
        });

        $seeder = new SchoolSeeder();
        $seeder->run();
        
        Schema::create('member_has_schools',function(Blueprint $table) {
            $table->unsignedBigInteger('member_id');
            $table->unsignedBigInteger('school_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
        Schema::dropIfExists('member_has_schools');
    }
}
