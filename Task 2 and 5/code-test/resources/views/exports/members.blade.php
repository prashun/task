<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Associated Schools</th>
    </tr>
    </thead>
    <tbody>
    @foreach($members as $member)
        <tr>
            <td>{{ $member->name }}</td>
            <td>{{ $member->email }}</td>
            <td>{{ $member->school_name }}</td>
        </tr>
    @endforeach
    </tbody>
</table>