<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class MembersExport implements FromView
{
    private $members;

    public function __construct($members)
    {
        $this->members = $members;
    }
    
    public function view(): View
    {
        return view('exports.members', [
            'members' => $this->members
        ]);
    }
}
