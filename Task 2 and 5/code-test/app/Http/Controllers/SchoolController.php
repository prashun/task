<?php

namespace App\Http\Controllers;

use App\Exports\MembersExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Repository\SchoolRepository;

class SchoolController extends Controller
{
    private $repo;

    public function __construct(SchoolRepository $repo){
        $this->repo = $repo;
    }

    public function searchBySchool(){
        $schools = $this->repo->get();
        return view('school-search',compact('schools'));
    }

    public function members(Request $request){
        if(!isset($request->school)){
            return redirect()->back();
        }

        $members = $this->repo->membersBySchool($request->school);
        $data = array(
            'members' => $members,
            'school_id' => $request->school
        );
        return view('school-members',$data);

    }

    public function export($school_id){
        $members = $this->repo->membersBySchool($school_id);
        
        $export = new MembersExport($members);
        return Excel::download($export, 'members.csv');
    }

        
}