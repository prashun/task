SELECT e1.UserRefID, GROUP_CONCAT(e1.emailaddress,' ') as emails
FROM emails e1
INNER JOIN profiles p ON e1.UserRefID = p.UserRefID
WHERE p.Deceased = 0
AND EXISTS (
    SELECT 1
    FROM emails e2
    WHERE e2.UserRefID = e1.UserRefID
      AND e2.emailaddress != e1.emailaddress
) 
group by e1.userRefID
having SUM(e1.Default = 1) > 0;